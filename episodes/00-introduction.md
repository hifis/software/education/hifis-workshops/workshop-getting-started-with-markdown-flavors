---
tags: #markdown #md
title: Introduction to Markdown
---


# Overview

- [Standard Syntax][stdmd]
- [Gitlab Flavor][gitlabmd]
- [Hedgedoc Flavor][hedgedocmd]

# Introduction

- First developed by __John Gruber__ and __Aaron Swartz__ in 2004
- Light weighed Markup Language
- Goal is __easy-to-read__ and __easy-to-write__ plain text => readability

# Standardization

- Many flavors/dialects popped up:
    
    - git platforms (github,[gitlab][gitlab-fmd],bitbucket)
    - SE focused sites (stackoverflow, stack exchanged, openstreet map, source forge)
    - tools (pandoc,kramdown)
    - documentation in code (python, c, c++, ...)

- From 2012, a [group of people][md-taskforce], including Jeff Atwood and [John MacFarlane][farlane] (Professor of Philosophy)
- Since 2014 Markdown flavors should be referred to as CommonMark
- [Standard](https://spec.commonmark.org/) efforts, no version 1 has been released yet.

# Tools

See https://www.markdownguide.org/tools/


<!-- Links -->
[gitlab-fmd]: https://docs.gitlab.com/ee/user/markdown.html
[farlane]: https://johnmacfarlane.net/index.html
[md-taskforce]: https://markdown.github.io/
[md-tools]: https://www.markdownguide.org/tools/
[stdmd]: 01-standard-syntax.md
[gitlabmd]: 03-gitlabmd.md
[hedgedocmd]: 04-gitlabmd.md
[hedgedoc-fmd]: https://docs.hedgedoc.org/references/hfm/
