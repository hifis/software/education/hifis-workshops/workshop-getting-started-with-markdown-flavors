# License

Copyright (c) 2022 Thomas Förster

This work is licensed under a [MIT](LICENSES/MIT.txt) license.

Please see the individual files for more accurate information.

> **Hint:** We provide the copyright and license information in accordance with the [REUSE Specification 3.0](https://reuse.software/spec/).
